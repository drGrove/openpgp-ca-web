+++
title = "OpenPGP CA at Free Software Foundation Europe"
keywords = ["fsfe"]
author = "Heiko"
date = 2021-02-24T10:30:00+01:00
banner = "empty.jpg"
+++

<!--
SPDX-FileCopyrightText: 2019-2021 Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: GPL-3.0-or-later
-->

# An OpenPGP CA instance at FSFE

Over the past months, we have worked with
[Free Software Foundation Europe (FSFE)](https://fsfe.org/) to set up an 
instance of the OpenPGP CA software as a part of FSFE's infrastructure - 
and to integrate its functionality with FSFE's community database.

<!--more-->

The starting point for the collaboration was that FSFE were evaluating how to
roll out a 
[Web Key Directory (WKD)](https://tools.ietf.org/html/draft-koch-openpgp-webkey-service-11)
to publish their community members' OpenPGP keys. OpenPGP CA was a natural
fit.  OpenPGP CA manages a group's OpenPGP keys.  And, publishing them to 
a WKD is functionality that OpenPGP CA supports out of the box.

From there, we proceeded to explore more possibilities, in particular how to 
apply OpenPGP CA's main concept at FSFE: certifying the
OpenPGP public keys that the CA manages.
Then, anyone who considers the FSFE CA to be a reasonable
trust anchor can configure their software to use the FSFE CA to
authenticate keys.

For example, if you configure GnuPG to rely on the
FSFE CA, you will automatically have an authenticated path to the keys of
all FSFE community members who participate in the system. Those keys will 
automatically show up as "trusted" when using GnuPG.
As long as you consider the procedures of the FSFE CA sufficient (and,
using GnuPG, you can limit the CA's scope to just FSFE community member keys),
you can configure your software rely on its certifications and never have to
worry about checking the fingerprint of FSFE community members OpenPGP
keys again.

The FSFE CA doesn't certify just any key.  It only certifies keys with
User IDs that have the form `username@fsfe.org`.  To make sure the key
is authentic, the user has to upload their key via the password
protected web portal.  If they don't have a password for `username`,
they can't upload a key for `username@fsfe.org`.  So, it's a pretty
good check.

This approach is very similar to how the
well known [Let's Encrypt](https://letsencrypt.org/) service works.
Both OpenPGP CA and Let's Encrypt are
fully automated systems that certify cryptographic identities based on
the ability to control a service associated with the identity.  In the
FSFE's case, that's the user's account.  In Let's Encrypt's case, it's
the domain's DNS or web presence.

# Collaboration

We've been working intensely on the project with [Max](https://fsfe.org/about/people/mehl/mehl.en.html) and
[Reinhard](https://wiki.fsfe.org/Supporters/reinhard) from the FSFE
for half a year.  The collaboration was a pleasure. Max and Reinhard were 
very helpful and had a lot of great feedback, which helped improve OpenPGP CA.

We hope to continue this type of collaboration with other organizations.
From the beginning, the design and development of OpenPGP CA has been driven by
collaboration with organizations who are planning to adopt our tools.
And, OpenPGP CA has profited greatly from those conversations.
If your organization is interested in introducing OpenPGP CA, we'd be
happy to help! This could be as simple as discussing how to deploy
OpenPGP CA in your organization, or we could help integrate OpenPGP CA
into your infrastructure as we did with the FSFE.

(To get in touch you can contact <heiko@schaefer.name>,
[68C8B3725276BEBB3EEA0E208ACFC41124CCB82E](https://openpgp-ca.org/heiko.asc)).

# Some technical details

Early on, we decided that OpenPGP CA's functionality should be accessed 
by FSFE's
[community database frontend](https://git.fsfe.org/fsfe-ams/fsfe-cd-front)
via a REST interface. Over the course of this project, we co-designed a 
[new REST API](/doc/restd/) for this purpose.
Our overall goal was to build a system that exposes the complexity - and 
the power - of OpenPGP in a user friendly way.

Also, we wanted to improve on common pain points around OpenPGP usage.
For example: FSFE will notify users about upcoming expiration of 
their OpenPGP keys. While it is good practice to configure keys with a 
limited expiration, traditionally there are no mechanisms to remind users 
that their key is expiring soon.

Internally, OpenPGP CA relies heavily on
[Sequoia PGP](https://sequoia-pgp.org/). 
This well-designed and [meticulously documented](https://docs.sequoia-pgp.org/sequoia_openpgp/) library makes writing
higher order tools that deal with OpenPGP artifacts (such as OpenPGP CA) 
easier and safer than it has ever been.  One of its main strengths is
that although the API tries to be as policy-free as possible, the API still
guides the user towards safe constructs.


# Towards making strong, federated authentication the new normal

OpenPGP offers powerful, decentralized mechanisms for users (and the 
users' software) to reason about the keys of their communication partners.
These mechanisms, which are geared towards user-sovereign authentication, set 
OpenPGP apart from other solutions in the field of cryptographic
communication.

We believe that an important step towards the future of federated, 
decentralized cryptographic communication is to improve tooling to

- help populate the "web-of-trust" with meaningful certifications that 
  users can conveniently rely on, and
- improve client side software that interprets these artifacts on behalf 
  of users (particularly email software that displays the authentication
  status of communication partners in its UI).

The first of these two points is the central mission of the OpenPGP CA 
project. We're proud and happy to have FSFE as both a user of our
software and as a partner who shares this vision.
