+++
title = "Usage in Kubernetes"
keywords = ["OpenPGP CA", "tutorial", "kubernetes"]
weight = 82
+++

<!--
SPDX-FileCopyrightText: 2019-2021 Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: GPL-3.0-or-later
-->

# Running in Kubernetes

You can also use `openpgp-ca-restd` in [Kubernetes][k8s].

The OpenPGP CA repository contains [kustomizations][kustomize] as `/kustomize/`
that helps you use the `openpgp-ca-restd` server in k8s.

To get started with deploying the `openpgp-ca-restd` server you will need to 
create a kustomization file:

```yaml
# kustomization.yaml

apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
namespace: openpgp-ca
resources:
  - git@gitlab.com:drGrove/openpgp-ca/kustomize/restd?ref=kustomize
```

You will also need to patch in the domain to ensure the CA is properly configured

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: openpgp-ca
spec:
  template:
    spec:
      initContainers:
        - name: init
          env:
            - name: DOMAIN
              value: "example.com"
```

All code is available [here](https://gitlab.com/openpgp-ca/openpgp-ca/-/tree/master/kustomize/example).

# Exposing to admins

By default `openpgp-ca-restd` should not be available to the outside world.

When it comes to accessing it as a kuberentes administrator you can simply
port-forward but you may want to give someone access to the CA but no give them
access to your cluster. With that in mind you can provide `openpgp-ca-restd` with
a domain and protect it with MutualTLS (mtls).

Any MutualTLS provider can work and should integrate with 
[Nginx Ingress Controller][ingress-nginx] pretty seemlessly. In this example
we will be using [mtls-server][mtls-server]. If you'd also like to set up an
`mtls-server` please see the docs there to configure.

```yaml
# ingress.yaml
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: openpgp-ca
  labels:
    app.kubernetes.io/name: openpgp-ca
  annotations:
    kubernetes.io/ingress.class: nginx # Use Nginx Ingress Controller
    cert-manager.io/cluster-issuer: letsencrypt # Get certificate using Cert-Manager
    nginx.ingress.kubernetes.io/auth-tls-verify-client: "on" # Enable mTLS
    nginx.ingress.kubernetes.io/auth-tls-secret: "mtls/mtls-certs" # Location of MTLS Certificate
    nginx.ingress.kubernetes.io/auth-tls-verify-depth: "1"
    nginx.ingress.kubernetes.io/auth-tls-pass-certificate-to-upstream: "false"
spec:
  tls:
    - hosts:
      - openpgp-ca.example.com
      secretName: opepgp-ca-tls
  rules:
    - host: openpgp-ca.example.com
      http:
        paths:
          - path: "/"
            backend:
              serviceName: openpgp-ca
              servicePort: http
```

All code is available [here](https://gitlab.com/openpgp-ca/openpgp-ca/-/tree/master/kustomize/example_with_ingress).

[k8s]: https://kubernetes.io/
[kustomize]: https://kustomize.io/
[ingress-nginx]: https://kubernetes.github.io/ingress-nginx
[mtls-server]: https://github.com/drGrove/mtls-server
